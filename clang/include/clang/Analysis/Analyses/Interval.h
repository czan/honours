//===- Interval.h - Integer Interval Analysis for Source CFGs     -*- C++ --*-//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements Live Variables analysis for source-level CFGs.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_INTERVAL_ANALYSIS_H
#define LLVM_CLANG_INTERVAL_ANALYSIS_H

#include "clang/Analysis/AnalysisContext.h"
#include "clang/AST/Decl.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/ImmutableSet.h"

#include "clang/Analysis/Analyses/IntervalSolver/Complete.hpp"

#include <vector>
#include <map>
#include <string>


typedef Complete<int64_t> ZBar;
template<>
inline ZBar infinity() {
  return ZBar(1, true);
}

namespace clang {

class CFG;
class CFGBlock;
class Stmt;
class DeclRefExpr;
class SourceManager;

typedef std::vector<std::map<std::string,int> > ConstraintMatrix; 
  

class IntervalAnalysis : public ManagedAnalysis {
public:

  IntervalAnalysis(AnalysisDeclContext &analysisContext);
  virtual ~IntervalAnalysis();

  std::map<CFGBlock*,std::vector<ZBar> > runOnAllBlocks(const Decl&, const ConstraintMatrix&);
  
  static IntervalAnalysis *create(AnalysisDeclContext &analysisContext) {
    return new IntervalAnalysis(analysisContext);
  }
  
  static const void *getTag();
  
private:
  AnalysisDeclContext* context;
};
  
} // end namespace clang

#endif
