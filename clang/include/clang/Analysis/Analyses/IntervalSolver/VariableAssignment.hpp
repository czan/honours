#ifndef VARIABLE_ASSIGNMENT_HPP
#define VARIABLE_ASSIGNMENT_HPP

#include "Expression.hpp"
#include "IdMap.hpp"

template<typename Domain>
struct VariableAssignment {
  virtual ~VariableAssignment() { }
  virtual const Domain operator[](const Variable<Domain>& x) = 0;
};

#include "EquationSystem.hpp"

template<typename Domain>
struct DynamicMaxStrategy;

template<typename Domain>
struct DynamicVariableAssignment : public VariableAssignment<Domain> {
  DynamicVariableAssignment(
    const EquationSystem<Domain>& system,
    DynamicMaxStrategy<Domain>& strat,
    const Domain& value=infinity<Domain>()
  ) : _system(system),
      _strategy(strat),
      _values(system.variableCount(), value),
      _old_values(system.variableCount(), value),
      _unstable(system.variableCount()),
      _influence(system.variableCount(),
                 IdSet<Variable<Domain> >(system.variableCount())),
      _touched(system.variableCount())
  { }

  const Domain operator[](const Variable<Domain>& var) {
    solve(var);
    return _values[var];
  }

  void invalidate(const Variable<Domain>& x) {
    if (!_unstable.contains(x)) {
      solver_log::fixpoint << indent() << "Invalidating " << x << std::endl;
 
      _unstable.insert(x);
      _touched.insert(x);
      _old_values[x] = _values[x];
      _values[x] = infinity<Domain>();

      IdSet<Variable<Domain> > infl = _influence[x];
      _influence[x].clear();

      for (typename IdSet<Variable<Domain> >::iterator
	     it = infl.begin(),
	     ei = infl.end();
	   it != ei;
	   ++it) {
	invalidate(_system.variable(*it));
      }
    }
  }

  IdSet<Variable<Domain> > get_changed() {
    IdSet<Variable<Domain> > changed;
    for (typename IdSet<Variable<Domain> >::iterator
           it = _touched.begin(),
           ei = _touched.end();
         it != ei;
         ) {
      Variable<Domain>& var = _system.variable(*it);
      if (!_unstable.contains(var) && _old_values[var] != _values[var]) {
        changed.insert(var);
        it++;
        _touched.remove(var);
      } else {
        it++;
      }
    }
    //_touched.clear();
    return changed;
  }

private:

  void solve(const Variable<Domain>& x) {
    if (_unstable.contains(x)) {
      _unstable.remove(x);
      solver_log::fixpoint << indent() << "Stabilise " << x << std::endl;

      stack_depth++;
      // we don't want the assignment to affect the strategy, so we're
      // going to use a const reference here
      const DynamicMaxStrategy<Domain>& const_strat = _strategy;
      DependencyAssignment assignment(*this, x);
      Domain val = _system[x]->eval(assignment, const_strat);
      stack_depth--;

      if (val != _values[x]) {
        solver_log::fixpoint << x << " = " << val << std::endl;
 
        IdSet<Variable<Domain> > oldInfluence = _influence[x];
        _influence[x].clear();
        _values[x] = val;

        _unstable.absorb(oldInfluence);

        for (typename IdSet<Variable<Domain> >::iterator it = oldInfluence.begin();
             it != oldInfluence.end();
             ++it) {
          solve(_system.variable(*it));
        }
      } else {
        solver_log::fixpoint << indent() << x << " did not change: "
		      << x << " = " << val << std::endl;
      }
    } else {
      solver_log::fixpoint << indent() << x << " is stable: "
		    << x << " = " << _values[x] << std::endl;
    }
  }

  struct DependencyAssignment : public VariableAssignment<Domain> {
    DependencyAssignment(DynamicVariableAssignment& assignment, const Variable<Domain>& var)
      : _assignment(assignment), _var(var) { }
    const Domain operator[](const Variable<Domain>& x) {
      const Domain result = _assignment[x];
      _assignment._influence[x].insert(_var);
      return result;
    }
  private:
    DynamicVariableAssignment& _assignment;
    const Variable<Domain>& _var;
  };

  const EquationSystem<Domain>& _system;
  DynamicMaxStrategy<Domain>& _strategy;
  IdMap<Variable<Domain>, Domain> _values;
  IdMap<Variable<Domain>, Domain> _old_values;
  IdSet<Variable<Domain> > _unstable;
  IdMap<Variable<Domain>,IdSet<Variable<Domain> > > _influence;
  IdSet<Variable<Domain> > _touched;
};

#endif
