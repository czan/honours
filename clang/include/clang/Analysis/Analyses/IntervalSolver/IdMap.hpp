#ifndef ID_MAP_HPP
#define ID_MAP_HPP

#include <cassert>
#include <ostream>

template<typename T, typename V>
struct IdMap {
  IdMap(unsigned int length, V initial)
    : _length(length), _assignment(new V[length]) {
    for (unsigned int i = 0; i < _length; ++i) {
      _assignment[i] = initial;
    }
  }
  IdMap(const IdMap& other)
    : _length(other._length), _assignment(new V[other._length]) {
    for (unsigned int i = 0; i < _length; ++i) {
      _assignment[i] = other._assignment[i];
    }
  }
  virtual ~IdMap() {
    delete[] _assignment;
  }
  virtual IdMap& operator=(const IdMap& other) {
    if (_length != other._length) {
      delete[] _assignment;
      _length = other._length;
      _assignment = new V[_length];
    }
    for (unsigned int i = 0; i < _length; ++i) {
      _assignment[i] = other._assignment[i];
    }
    return *this;
  }
  virtual const V& operator[] (const T& x) const {
    assert(x.id() < _length);
    return _assignment[x.id()];
  }
  virtual V& operator[] (const T& x) {
    assert(x.id() < _length);
    return _assignment[x.id()];
  }

  virtual bool operator==(const IdMap& other) const {
    if (_length != other._length)
      return false;
    for (unsigned int i = 0; i < _length; ++i) {
      if (_assignment[i] != other._assignment[i]) {
        return false;
      }
    }
    return true;
  }
  virtual bool operator!=(const IdMap& other) const {
    return !(*this == other);
  }

  template<typename Q,typename Z>
  friend std::ostream& operator<<(std::ostream& cout, const IdMap<Q, Z>& rho);

  protected:
  unsigned int _length;
  V* _assignment;
};

template<typename T, typename V>
std::ostream& operator<<(std::ostream& cout, const IdMap<T,V>& rho) {
  cout << "{";
  for (unsigned int i = 0; i < rho._length; ++i) {
    if (i != 0) cout << ", ";
    cout << i << ": " << rho._assignment[i];
  }
  cout << "}";
  return cout;
}

#endif
