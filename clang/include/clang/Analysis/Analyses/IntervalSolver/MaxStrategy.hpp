#ifndef MAX_EXPRESSION_HPP
#define MAX_EXPRESSION_HPP

#include <ostream>
#include "Expression.hpp"
#include "EquationSystem.hpp"
#include "IdSet.hpp"

template<typename Domain>
struct MaxStrategy {
  virtual ~MaxStrategy() { }
  virtual unsigned int get(const MaxExpression<Domain>& e) {
    return const_cast<const MaxStrategy<Domain>*>(this)->get(e);
  }
  virtual unsigned int get(const MaxExpression<Domain>& e) const = 0;
};

unsigned int stack_depth = 1;

std::string indent() {
  std::string result = "";
  for (unsigned int i = 0; i < stack_depth; ++i) {
    result += '\t';
  }
  return result;
}

#include "VariableAssignment.hpp"

template<typename Domain>
struct DynamicVariableAssignment;

template<typename Domain>
struct DynamicMaxStrategy : public MaxStrategy<Domain> {
  DynamicMaxStrategy(
    const EquationSystem<Domain>& system
  ) : _system(system),
      _rho(NULL),
      _values(system.maxExpressionCount(), 0),
      _stable(system.maxExpressionCount()),
      _influence(system.maxExpressionCount(),
                 IdSet<MaxExpression<Domain> >(system.maxExpressionCount()))
  {}

  void setRho(DynamicVariableAssignment<Domain>& rho) {
    _rho = &rho;
  }

  unsigned int get(const MaxExpression<Domain>& e) const {
    solver_log::strategy << indent() << "Look up " << e << std::endl;
    return _values[e];
  }

  unsigned int get(const MaxExpression<Domain>& e) {
    solver_log::strategy << indent() << "Solve for " << e << std::endl;
    solve(e);
    return _values[e];
  }

  void invalidate(const MaxExpression<Domain>& v) {
    if (_stable.contains(v)) {
      solver_log::strategy << indent() << "Invalidating " << v << std::endl; 
      //solver_log::debug << indent() << "  influence sets " << _influence << std::endl;
      _stable.remove(v);
      
      IdSet<MaxExpression<Domain> > infl = _influence[v];
      for (typename IdSet<MaxExpression<Domain> >::iterator
             it = infl.begin(),
             end = infl.end();
           it != end;
           ++it) {
        invalidate(_system.maxExpression(*it));
      }
    }
  }

private:
  void solve(const MaxExpression<Domain>& x) {
    if (!_stable.contains(x)) {
      _stable.insert(x);
      solver_log::strategy << indent() << "Stabilise " << x << std::endl;

      stack_depth++; 
      DependencyAssignment assignment(*this, *_rho, x);
      DependencyStrategy depStrat(*this, x);
      unsigned int val = x.bestStrategy(assignment, depStrat, _values[x]);
      stack_depth--;

      if (val != _values[x]) {
        solver_log::strategy << x << " => " << *x.arguments()[val] << std::endl;

        _values[x] = val;

        _rho->invalidate(*_system.varFromExpr(x));
        
	//_rho->stabilise();

        IdSet<MaxExpression<Domain> > infl = _influence[x];
        _stable.filter(infl); 
        
        for (typename IdSet<MaxExpression<Domain> >::iterator
               it = infl.begin(),
               end = infl.end();
             it != end;
             ++it) {
          solve(_system.maxExpression(*it));
        }
      } else {
        solver_log::strategy << indent() << x << " did not change: "
		      << x << " => " << *x.arguments()[val] << std::endl;
      }
    } else {
      solver_log::strategy << indent() << x << " is stable: "
		    << x << " => " << *x.arguments()[_values[x]] << std::endl;
    }
  }

  struct DependencyAssignment : public VariableAssignment<Domain>{
    DependencyAssignment(DynamicMaxStrategy& strat,
                         DynamicVariableAssignment<Domain>& rho,
                         const MaxExpression<Domain>& expr)
      : _strat(strat),
        _rho(rho),
        _expr(expr),
	_current(strat._system.variableCount()) {
    }
    const Domain operator[](const Variable<Domain>& var) {
      // force evaluation to get touched variables
      Domain value = _rho[var];

      _strat._influence[*_strat._system[var]].insert(_expr);

      // invalidate touched variables
      IdSet<Variable<Domain> > changed = _rho.get_changed();
      for (typename IdSet<Variable<Domain> >::iterator
             it = changed.begin(),
             ei = changed.end();
           it != ei;
           ++it) {
        _strat.invalidate(*_strat._system[_strat._system.variable(*it)]);
      }
      return value;
    }
  private:
    DynamicMaxStrategy& _strat;
    DynamicVariableAssignment<Domain>& _rho;
    const MaxExpression<Domain>& _expr;
    IdSet<Variable<Domain> > _current;
  };

  struct DependencyStrategy : public MaxStrategy<Domain> {
    DependencyStrategy(DynamicMaxStrategy& strat, const MaxExpression<Domain>& expr)
      : _strat(strat),
        _expr(expr) {
    }
    unsigned int get(const MaxExpression<Domain>& e) const {
      _strat._influence[e].insert(_expr);
      return _strat._values[e];
    }
    unsigned int get(const MaxExpression<Domain>& e) {
      _strat.solve(e);
      _strat._influence[e].insert(_expr);
      return _strat._values[e];
    }
  private:
    DynamicMaxStrategy& _strat;
    const MaxExpression<Domain>& _expr;
  };

private: 
  const EquationSystem<Domain>& _system;
  DynamicVariableAssignment<Domain>* _rho;
  IdMap<MaxExpression<Domain>,unsigned int> _values;
  IdSet<MaxExpression<Domain> > _stable;
  IdMap<MaxExpression<Domain>,IdSet<MaxExpression<Domain> > > _influence;
};


template<typename Domain>
struct Solver {
  Solver(const EquationSystem<Domain>& system)
    : _system(system),
      _strategy(system),
      _rho(_system, _strategy, -infinity<Domain>()) {
    _strategy.setRho(_rho);
  }

  Domain solve(Variable<Domain>& var) {
    MaxExpression<Domain>& rhs = *_system[var];
    // this will automatically work sufficiently to get the final
    // strategy for this variable's RHS
    _strategy.get(rhs);

    // this will automatically solve for the value of the RHS, if required
    return _rho[var];
  }
private:
  const EquationSystem<Domain>& _system;
  DynamicMaxStrategy<Domain> _strategy;
  DynamicVariableAssignment<Domain> _rho;
};


/*template<typename Domain>
std::ostream& operator<<(std::ostream& cout, const MaxStrategy<Domain>& strat) {
  strat.print(cout);
  return cout;
}*/

#endif
