#ifndef LOG_HPP
#define LOG_HPP

#include <string>
#include <iostream>
#include <map>
#include <cstdio>

namespace solver_log {

  struct Logger : public std::ostream {
    Logger(std::streambuf* buffer, const std::string& name)
      : std::ostream(NULL) { }
  };

  Logger strategy(std::cerr.rdbuf(), "strategy");
  Logger fixpoint(std::cerr.rdbuf(), "fixpoint");
  Logger debug(std::cerr.rdbuf(), "debug");

}

#endif
