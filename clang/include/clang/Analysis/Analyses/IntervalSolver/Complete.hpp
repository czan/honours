#ifndef COMPLETE_HPP
#define COMPLETE_HPP

#include <cassert>
#include <ostream>
#include <istream>
#include <limits>

template<typename T>
T infinity();

template<>
inline double infinity() {
  return std::numeric_limits<double>::infinity();
}


template<typename T>
struct Complete {
  Complete()
    : _value(0), _infinity(false) { }
  Complete(const T& value)
    : _value(value), _infinity(false) { }
  Complete(const T& value, const bool& infinity)
    : _value(value), _infinity(infinity) {
    assert(value != 0 || infinity == false);
  }
  Complete(const Complete& other)
    : _value(other._value), _infinity(other._infinity) { }

  Complete& operator=(const Complete& other) {
    _value = other._value;
    _infinity = other._infinity;
    return *this;
  }
  Complete& operator+=(const Complete& other) {
    return (*this) = (*this) + other;
  }
  Complete& operator-=(const Complete& other) {
    return (*this) = (*this) - other;
  }
  Complete& operator*=(const Complete& other) {
    return (*this) = (*this) * other;
  }

  Complete operator-() const {
    return Complete<T>(- _value, _infinity);
  }
  Complete operator+(const Complete& other) const {
    if (_infinity && other._infinity) {
      if (_value > 0 || other._value > 0)
        return Complete<T>(1, true);
      return Complete<T>(-1, true);
    } else if (_infinity) {
      return *this;
    } else if (other._infinity) {
      return other;
    } else {
      return Complete(_value + other._value, false);
    }
  }
  Complete operator-(const Complete& other) const {
    return *this + (- other);
  }
  Complete operator*(const Complete& other) const {
    return Complete(_value * other._value, (_infinity || other._infinity));
  }

  bool operator!() const {
    return _value == 0;
  }
  bool operator<(const Complete& other) const {
    if (*this == other)
      return false;
    if (_infinity) {
      return _value < 0;
    } else if (other._infinity) {
      return other._value > 0;
    } else {
      return _value < other._value;
    }
  }
  bool operator>=(const Complete& other) const {
    return !(*this < other);
  }
  bool operator>(const Complete& other) const {
    return other < *this;
  }
  bool operator<=(const Complete& other) const {
    return !(*this > other);
  }
  bool operator==(const Complete& other) const {
    if (_infinity) {
      return other._infinity && ((_value < 0 && other._value < 0) ||
                                 (_value > 0 && other._value > 0));
    } else {
      return !other._infinity && (_value == other._value);
    }
  }
  bool operator!=(const Complete& other) const {
    return !(*this == other);
  }

  template<typename Z>
  friend std::istream& operator<<(std::istream&, Complete<Z>&);
  template<typename Z>
  friend std::ostream& operator<<(std::ostream&, const Complete<Z>&);

  template<typename S>
  S as() const {
    if (_infinity) {
      if (_value > 0)
        return infinity<S>();
      return -infinity<S>();
    }
    return (S) _value;
  }

  private:
  T _value;
  bool _infinity;
};

template<typename Z>
std::istream& operator>>(std::istream& cin, Complete<Z>& num) {
  Z value;
  cin >> value;
  num = Complete<Z>(value, false);
  return cin;
}

template<typename Z>
std::ostream& operator<<(std::ostream& cout, const Complete<Z>& num) {
  if (num._infinity) {
    cout << (num._value > 0 ? "inf" : "-inf");
  } else {
    cout << num._value;
  }
  return cout;
}

template<>
inline Complete<int> infinity() {
  return Complete<int>(1, true);
}

#endif
