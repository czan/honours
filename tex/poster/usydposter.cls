%%
%% This is file `usydposter.cls'
%% 
%% Author: Tim Dawborn <tim.dawborn@sydney.edu.au>
%%
\ProvidesClass{usydposter}[2010/05/08]
\NeedsTeXFormat{LaTeX2e}
\LoadClass{article}

\newlength{\poster@top}
\newlength{\poster@bottom}
\newlength{\poster@left}
\newlength{\poster@right}

\newlength{\poster@titleboxlpad}
\newlength{\poster@titleboxheight}
\newlength{\poster@logoboxwidth}
\newlength{\poster@logoboxheight}

\newlength{\poster@titlelpad}
\newlength{\poster@titlerpad}
\newlength{\poster@titletpad}
\newlength{\poster@titleheight}
\newlength{\poster@logowidth}
\newlength{\poster@logoheight}
\newlength{\poster@logopad}

\newlength{\poster@textpad}

% bit of a hack to get portrait/landscape mode going
\def\poster@direction{portrait}
\def\poster@blue{usydblue}

\setlength{\poster@top}{1.25cm}
\setlength{\poster@bottom}{1.25cm}
\setlength{\poster@left}{1.49cm}  %15mm + \poster@textpad
\setlength{\poster@right}{1.49cm} %15mm + \poster@textpad

% the indentation of the outer columns from the edge
\setlength{\poster@textpad}{1.34cm}

% the red box around the logo
\setlength{\poster@logoboxwidth}{11.81cm}
\setlength{\poster@logoboxheight}{5.91cm}

% the logo itself
\setlength{\poster@logowidth}{115mm}
\setlength{\poster@logopad}{0mm}

% the blue box
\setlength{\poster@titleboxheight}{10cm}
\setlength{\poster@titleboxlpad}{5.79cm}

% the text box for the header text
\setlength{\poster@titlelpad}{12.68cm}
\setlength{\poster@titlerpad}{1.57cm}
\setlength{\poster@titletpad}{0.80cm}
\setlength{\poster@titleheight}{9.4cm}

% space between the columns
\setlength{\columnsep}{15mm}

\DeclareOption{portrait}{\relax}
\DeclareOption{landscape}{%
  \def\poster@direction{landscape}
}
\DeclareOption{conference}{%
  \def\poster@direction{landscape}
  \setlength{\poster@top}{10mm}
  \setlength{\poster@bottom}{10mm}
  \setlength{\poster@left}{25mm}
  \setlength{\poster@right}{25mm}
  \setlength{\columnsep}{20mm}
}
\DeclareOption{engineering}{%
  \def\poster@blue{usydlblue}
}
\ProcessOptions

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{calc}
\usepackage{color}
\usepackage{fontenc}
\usepackage[paper=a1paper,\poster@direction,noheadfoot,ignoreall,centering,top=\poster@top,left=\poster@left,right=\poster@right,bottom=\poster@bottom]{geometry}
\usepackage{pgf}
\usepackage{multicol}
\usepackage{tipa}

\usepackage{colours}

% page style
\pagestyle{empty}
\pagecolor[rgb]{1.0,1.0,1.0}
\color{black}

% fonts
\renewcommand{\encodingdefault}{T1}
\renewcommand{\familydefault}{phv}
\renewcommand{\seriesdefault}{m}
\renewcommand{\shapedefault}{n}
\usefont{T1}{phv}{m}{n}

% font sizes
\renewcommand{\tiny}{\fontsize{12}{14}\selectfont}
\renewcommand{\scriptsize}{\fontsize{14.4}{18}\selectfont}   
\renewcommand{\footnotesize}{\fontsize{17.28}{22}\selectfont}
\renewcommand{\small}{\fontsize{20.74}{23}\selectfont}
\renewcommand{\normalsize}{\fontsize{23}{25}\selectfont}
\renewcommand{\large}{\fontsize{28}{00}\selectfont}
\renewcommand{\Large}{\fontsize{35.83}{45}\selectfont}
\renewcommand{\LARGE}{\fontsize{43}{54}\selectfont}
\renewcommand{\huge}{\fontsize{51.6}{64}\selectfont}
\renewcommand{\Huge}{\fontsize{61.92}{77}\selectfont}

\pgfdeclareimage[width=\poster@logowidth]{logo}{usyd-primary-colour-reversed}
\settoheight{\poster@logoheight}{\pgfuseimage{logo}}

% page numbering
\pagenumbering{arabic} % needed even though will not be used

% sections
\renewcommand{\section}{\@startsection
    {section}{1}{0mm}%
    {-0.3\baselineskip}%
    {0.6\baselineskip}%
    {\fontsize{28}{30}\selectfont\bfseries\color{\poster@blue}}}
\renewcommand{\thesection}{\arabic{section}.}

\renewcommand{\subsection}{\@startsection
    {subsection}{2}{0mm}%
    {-0.5\baselineskip}%
    {0.3\baselineskip}%
    {\fontsize{24}{26}\selectfont\bfseries\color{black}}}
\renewcommand{\thesubsection}{\arabic{section}.\arabic{subsection}.}

% figures
\renewenvironment{figure}{%
    \stepcounter{figure}
    \renewcommand\caption[1]{\begin{center}\small\textbf{Figure \arabic{figure}:} \textit{##1}\end{center}}
  }{%
  }

% tables
\renewenvironment{table}{%
    \stepcounter{table}
    \renewcommand\caption[1]{\begin{center}\small\textbf{Table \arabic{table}:} \textit{##1}\end{center}}
    \begin{center}
  }{%
    \end{center}
  }

% references
\newcommand{\references}{\fontsize{20}{22}\selectfont}

% make the header
\newcommand{\makeheader}{%
  \def\titlefont  {\color{white}\fontsize{54}{60}\selectfont}
  \def\authorfont {\color{white}\fontsize{40}{40}\selectfont\itshape}
  \def\facultyfont{\color{white}\fontsize{28}{32}\selectfont}
  \def\emailfont  {\color{white}\fontsize{28}{32}\selectfont\ttfamily}
  \noindent\begin{pgfpicture}{0cm}{0cm}{\textwidth}{\poster@titleboxheight}
    % blue rectangle
    \color{\poster@blue}
    \pgfrect[fill]
      {\pgfpoint{\poster@titleboxlpad - \poster@textpad}{0cm}}
      {\pgfpoint{\textwidth - \poster@titleboxlpad + 2*\poster@textpad}{\poster@titleboxheight}}
    % red rectangle
    \color{usydred}
    \pgfrect[fill]
      {\pgfpoint{-\poster@textpad}{\poster@titleboxheight - \poster@logoboxheight}}
      {\pgfpoint{\poster@logoboxwidth}{\poster@logoboxheight}}
    % logo
    \color{black}
    \pgfputat
      {\pgfpoint{\poster@logopad - \poster@textpad}{\poster@titleboxheight - \poster@logoboxheight + \poster@logopad}}
      {\pgfuseimage{logo}}
    % poster title
    \pgfputat
      {\pgfpoint{\poster@titlelpad - \poster@textpad}{\poster@titleboxheight - \poster@titletpad}}
      {\pgfbox[left,top]{%
        \titlefont
        \parbox
          {\textwidth - \poster@titlerpad - \poster@titlelpad}
          {\raggedright\MakeUppercase{\postertitle}}}}
    % poster author
    \pgfputat
      {\pgfpoint{\poster@titlelpad - \poster@textpad}{3.2cm}}
      {\pgfbox[left,bottom]{%
        \authorfont
        \parbox
          {\textwidth - \poster@titlerpad - \poster@titlelpad}
          {\posterauthor}}}
    % poster faculty
    \pgfputat
      {\pgfpoint{\poster@titlelpad - \poster@textpad}{0.5cm}}
      {\pgfbox[left,bottom]{%
        \facultyfont
        \parbox
          {\textwidth - \poster@titlerpad - \poster@titlelpad}
          {\posterschool\newline\emailfont\posteremail}}}
  \end{pgfpicture}
}

% macros which users should redefine
\newcommand{\postertitle}{Title of your talk}
\newcommand{\posterauthor}{Joe Blogs}
\newcommand{\posterschool}{\textschwa-lab, School of Information Technologies}
\newcommand{\posteremail}{joe.blogs@sydney.edu.au}

\renewcommand{\title}[1]{\renewcommand{\postertitle}{#1}}
\renewcommand{\author}[1]{\renewcommand{\posterauthor}{#1}}
\newcommand{\school}[1]{\renewcommand{\posterschool}{#1}}
\newcommand{\email}[1]{\renewcommand{\posteremail}{#1}}

% vim: set ft=tex et ts=2 sts=2 sw=2:
