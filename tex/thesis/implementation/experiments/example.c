int example() {
  /* A */
  int x1 = 1;
  int x2 = 1;

  while (/* B */ x1 <= 8) {
    x1 = x1 + 2;
    x2 = x2 + 2;
  }
  /* C */
  return x2;
}

