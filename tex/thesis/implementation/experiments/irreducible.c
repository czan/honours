int irreducible() {
  int x = 1;
  while (x > 0) {
    if (x < 2)
      x = x + 1;
    x = x + 1;
  }
  return x;
}
