int fib() {
  int fib_last = 0;
  int fib = 1;
  int i = 0;
  while (i < 20) {
    int temp_fib = fib;
    fib = fib + fib_last;
    fib_last = temp_fib;
    i = i + 1;
  }
  return fib;
}

