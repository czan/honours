#!/usr/bin/python

import random

size = 10

def generate_variable(size):
  if random.randint(1,3) == 1:
    return "x" + str(random.randint(1, size))
  return str(random.randint(1,10))

def generate_expression(size):
  if random.randint(1, 5) < 3:
    operator = random.choice(['add', 'mul'])
    args = (generate_expression(size) for i in xrange(random.randint(2, 4)))
    return "%s(%s)" % (operator, ",".join(args))
  else:
    return generate_variable(size)

def generate_min_expression(size):
  return "min(" + ",".join(generate_expression(size) for i in xrange(random.randint(2, 3))) + ")"

def generate_max_expression(size):
  return "max(" + ",".join(generate_min_expression(size) for i in xrange(random.randint(2, 3))) + ")"


for i in xrange(size):
  print "x"+str(i+1) + " = " + generate_max_expression(size)
