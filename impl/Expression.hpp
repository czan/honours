#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include <string>
#include <vector>
#include <sstream>
#include <set>
#include "IdMap.hpp"
#include "Operator.hpp"

template<typename Domain>
struct VariableAssignment;

template<typename Domain>
struct MaxStrategy;

template<typename Domain>
struct Variable;

template<typename Domain>
struct MaxExpression;

template<typename Domain>
struct Expression {
  virtual ~Expression() { }

  virtual const MaxExpression<Domain>* toMaxExpression() const {
    return NULL;
  }

  virtual MaxExpression<Domain>* toMaxExpression() {
    return NULL;
  }

  virtual Domain eval(VariableAssignment<Domain>&) const = 0;
  virtual Domain eval(VariableAssignment<Domain>& rho,
                      const MaxStrategy<Domain>&) const {
    return eval(rho);
  }
  virtual Domain eval(VariableAssignment<Domain>& rho,
                      MaxStrategy<Domain>&) const {
    return eval(rho);
  }

  virtual void mapTo(Variable<Domain>&, IdMap<MaxExpression<Domain>, Variable<Domain>*>&) const {
  }

  virtual void subMaxExprs(std::set<const MaxExpression<Domain>*>&) const {
  }

  virtual void print(std::ostream&) const = 0;
};

template<typename Domain>
struct Constant : public Expression<Domain> {
  Constant(const Domain& value)
    : _value(value) { }

  virtual Domain eval(VariableAssignment<Domain>&) const {
    return _value;
  }

  void print(std::ostream& cout) const {
    cout << _value;
  }

  private:
  Domain _value;
};

template<typename Domain>
struct Variable : public Expression<Domain> {
  Variable(const unsigned int& id, const std::string& name)
    : _id(id), _name(name) { }

  unsigned int id() const {
    return _id;
  }
  std::string name() const {
    return _name;
  }

  virtual Domain eval(VariableAssignment<Domain>& rho) const {
    return rho[*this];
  }

  void print(std::ostream& cout) const {
    cout << _name;
  }

  private:
  const unsigned int _id;
  const std::string _name;
};

template<typename Domain>
struct OperatorExpression : public Expression<Domain> {
  OperatorExpression(const Operator<Domain>& op, const std::vector<Expression<Domain>*>& arguments)
    : _operator(op), _arguments(arguments) { }

  virtual Domain eval(VariableAssignment<Domain>& rho) const {
    std::vector<Domain> argumentValues;
    for (typename std::vector<Expression<Domain>*>::const_iterator it = _arguments.begin();
         it != _arguments.end();
         ++it) {
      argumentValues.push_back((*it)->eval(rho));
    }
    return _operator.eval(argumentValues);
  }

  virtual Domain eval(VariableAssignment<Domain>& rho, const MaxStrategy<Domain>& strat) const {
    std::vector<Domain> argumentValues;
    for (typename std::vector<Expression<Domain>*>::const_iterator it = _arguments.begin();
         it != _arguments.end();
         ++it) {
      argumentValues.push_back((*it)->eval(rho, strat));
    }
    return _operator.eval(argumentValues);
  }

  virtual Domain eval(VariableAssignment<Domain>& rho, MaxStrategy<Domain>& strat) const {
    std::vector<Domain> argumentValues;
    for (typename std::vector<Expression<Domain>*>::const_iterator it = _arguments.begin();
         it != _arguments.end();
         ++it) {
      argumentValues.push_back((*it)->eval(rho, strat));
    }
    return _operator.eval(argumentValues);
  }

  std::vector<Expression<Domain>*>& arguments() {
    return _arguments;
  }

  const std::vector<Expression<Domain>*>& arguments() const {
    return _arguments;
  }

  const Operator<Domain>& op() const {
    return _operator;
  }

  virtual void mapTo(Variable<Domain>& v, IdMap<MaxExpression<Domain>, Variable<Domain>*>& m) const {
    for (unsigned int i = 0, length = _arguments.size();
         i < length;
         ++i) {
      _arguments[i]->mapTo(v, m);
    }
  }

  virtual void subMaxExprs(std::set<const MaxExpression<Domain>*>& exprs) const {
    for (unsigned int i = 0, length = _arguments.size();
         i < length;
         ++i) {
      _arguments[i]->subMaxExprs(exprs);
    }
  }

  void print(std::ostream& cout) const {
    cout << _operator << "(";
    for (unsigned int i = 0, length = _arguments.size();
         i < length;
         ++i) {
      if (i > 0)
        cout << ", ";
      cout << *_arguments[i];
    }
    cout << ")";
  }

  private:
  const Operator<Domain>& _operator;
  protected:
  std::vector<Expression<Domain>*> _arguments;
};

template<typename Domain>
struct MaxExpression : public OperatorExpression<Domain> {
  MaxExpression(const unsigned int& id, const Maximum<Domain>& op, const std::vector<Expression<Domain>*>& arguments)
    : OperatorExpression<Domain>(op, arguments), _id(id) { }

  MaxExpression* toMaxExpression() {
    return this;
  }

  const MaxExpression* toMaxExpression() const {
    return this;
  }

  virtual Domain eval(VariableAssignment<Domain>& rho, const MaxStrategy<Domain>& strat) const {
    log::fixpoint << "const MaxExpression eval" << std::endl;
    return this->_arguments[strat.get(*this)]->eval(rho, strat);
  }

  virtual Domain eval(VariableAssignment<Domain>& rho, MaxStrategy<Domain>& strat) const {
    log::strategy << "const MaxExpression eval" << std::endl;
    return this->_arguments[strat.get(*this)]->eval(rho, strat);
  }

  unsigned int bestStrategy(VariableAssignment<Domain>& rho, MaxStrategy<Domain>& strat, unsigned int bestIndex) const {
    Domain bestValue = this->_arguments[bestIndex]->eval(rho, strat);

    for (unsigned int i = 0, length = this->_arguments.size();
         i < length;
         ++i) {
      const Domain value = this->_arguments[i]->eval(rho, strat);
      if (bestValue < value) {
        bestIndex = i;
        bestValue = value;
      }
    }
    return bestIndex;
  }

  virtual void mapTo(Variable<Domain>& v, IdMap<MaxExpression<Domain>,Variable<Domain>*>& m) const {
    m[*this] = &v;
    for (unsigned int i = 0, length = this->_arguments.size();
         i < length;
         ++i) {
      this->_arguments[i]->mapTo(v, m);
    }
  }

  virtual void subMaxExprs(std::set<const MaxExpression<Domain>*>& exprs) const {
    exprs.insert(this);
    for (unsigned int i = 0, length = this->_arguments.size();
         i < length;
         ++i) {
      this->_arguments[i]->subMaxExprs(exprs);
    }
  }

  unsigned int id() const {
    return _id;
  }

  private:
  const unsigned int _id;
};

template<typename T>
std::ostream& operator<<(std::ostream& cout, const Expression<T>& exp) {
  exp.print(cout);
  return cout;
}

#include "VariableAssignment.hpp"

#endif
