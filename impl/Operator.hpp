#ifndef OPERATOR_HPP
#define OPERATOR_HPP

#include <cassert>
#include <vector>

template<typename Domain>
struct Operator {
  virtual ~Operator() { }

  virtual Domain eval(const std::vector<Domain>&) const = 0;

  virtual void print(std::ostream&) const = 0;
};

template<typename Domain>
struct Maximum : public Operator<Domain> {
  virtual Domain eval(const std::vector<Domain>& arguments) const {
    Domain result = -infinity<Domain>();
    for (typename std::vector<Domain>::const_iterator it = arguments.begin();
         it != arguments.end();
         ++it) {
      result = (result < *it ? *it : result);
    }
    return result;
  }
  void print(std::ostream& cout) const {
    cout << "max";
  }
};

template<typename Domain>
struct Minimum : public Operator<Domain> {
  virtual Domain eval(const std::vector<Domain>& arguments) const {
    Domain result = infinity<Domain>();
    for (typename std::vector<Domain>::const_iterator it = arguments.begin();
         it != arguments.end();
         ++it) {
      result = (*it < result ? *it : result);
    }
    return result;
  }
  void print(std::ostream& cout) const {
    cout << "min";
  }
};

template<typename Domain>
struct Negation : public Operator<Domain> {
  virtual Domain eval(const std::vector<Domain>& arguments) const {
    assert(arguments.size() == 1);
    return -arguments[0];
  }
  void print(std::ostream& cout) const {
    cout << "-";
  }
};

template<typename Domain>
struct Addition : public Operator<Domain> {
  virtual Domain eval(const std::vector<Domain>& arguments) const {
    Domain result = 0;
    for (typename std::vector<Domain>::const_iterator it = arguments.begin();
         it != arguments.end();
         ++it) {
      result += *it;
    }
    return result;
  }
  void print(std::ostream& cout) const {
    cout << "add";
  }
};

template<typename Domain>
struct Subtraction : public Operator<Domain> {
  virtual Domain eval(const std::vector<Domain>& arguments) const {
    Domain result = 0;
    for (typename std::vector<Domain>::const_iterator it = arguments.begin();
         it != arguments.end();
         ++it) {
      if (it == arguments.begin())
        result = *it;
      else
        result -= *it;
    }
    return result;
  }
  void print(std::ostream& cout) const {
    cout << "sub";
  }
};

template<typename Domain>
struct Multiplication : public Operator<Domain> {
  virtual Domain eval(const std::vector<Domain>& arguments) const {
    Domain result = 1;
    for (typename std::vector<Domain>::const_iterator it = arguments.begin(),
           end = arguments.end();
         it != end;
         ++it) {
      result *= *it;
    }
    return result;
  }
  void print(std::ostream& cout) const {
    cout << "mult";
  }
};

template<typename Domain>
struct Comma : public Operator<Domain> {
  virtual Domain eval(const std::vector<Domain>& arguments) const {
    if (arguments[0] == -infinity<Domain>()) {
      return -infinity<Domain>();
    }
    return arguments[1];
  }
  void print(std::ostream& cout) const {
    cout << "comma";
  }
};

template<typename Domain>
struct Guard : public Operator<Domain> {
  virtual Domain eval(const std::vector<Domain>& arguments) const {
    Domain result = arguments[2];
    if (arguments[0] < arguments[1]) {
      result = -infinity<Domain>();
    }
    return result;
  }
  void print(std::ostream& cout) const {
    cout << "guard";
  }
};

#include "MCFSimplex.h"

template<typename Domain>
struct MinCostFlow : public Operator<Domain> {
  MinCostFlow(const std::vector<Domain>& supplies, const std::vector<std::pair<int,int> > arcs)
    : _supplies(supplies), _arcs(arcs), _solver(0,0) {
    MCFClass::FNumber* deficits = new MCFClass::FNumber[supplies.size()];
    MCFClass::Index* starts = new MCFClass::Index[arcs.size()];
    MCFClass::Index* ends = new MCFClass::Index[arcs.size()];

    for (int i = 0, size = supplies.size(); i < size; ++i) {
      deficits[i] = -supplies[i].template as<MCFClass::FNumber>();
    }
    for (int i = 0, size = arcs.size(); i < size; ++i) {
      starts[i] = arcs[i].first;
      ends[i] = arcs[i].second;
    }

    _solver.LoadNet(supplies.size(), arcs.size(), // max nodes/arcs
                    supplies.size(), arcs.size(), // current nodes/arcs
                    NULL, NULL, // arcs have inf cap, zero cost (to begin)
                    deficits, // deficits for each node
                    starts, ends); // start/end of each edge

    delete[] deficits;
    delete[] starts;
    delete[] ends;
  }
  Domain eval (const std::vector<Domain>& costs) const {
    assert(costs.size() == _arcs.size());
    if (costs.size() == 0)
      return 0;
    for (int i = 0, size = costs.size(); i < size; ++i) {
      _solver.ChgCost(i, costs[i].template as<MCFClass::CNumber>());
    }
    _solver.SolveMCF();

    if (_solver.MCFGetStatus() != MCFClass::kOK) {
      return -infinity<Domain>();
    } else {
      MCFClass::FONumber num = _solver.MCFGetFO();
      if (num == MCFClass::Inf<MCFClass::FONumber>()) {
        return infinity<Domain>();
      } else if (num == -MCFClass::Inf<MCFClass::FONumber>()) {
        return -infinity<Domain>();
      } else {
        if (((int)num) == -2147483648)
          return -infinity<Domain>();
        return num;
      }
    }
  }
  void print(std::ostream& cout) const {
    std::string supplyString = "[";
    for (int i = 0, size = _supplies.size(); i < size; ++i) {
      if (i > 0)
        supplyString += ",";
      std::stringstream stream;
      stream << _supplies[i];
      supplyString += stream.str();
    }
    supplyString += ']';

    std::string arcString = "[";
    for (int i = 0, size = _arcs.size(); i < size; ++i) {
      if (i > 0)
        arcString += ",";
      {
        std::stringstream stream;
        stream << _arcs[i].first;
        arcString += stream.str() + ":";
      }
      {
        std::stringstream stream;
        stream << _arcs[i].second;
        arcString += stream.str();
      }
    }
    arcString += ']';

    cout << "MCF<" << supplyString << "," << arcString << ">";
  }
private:
  std::vector<Domain> _supplies;
  std::vector<std::pair<int,int> > _arcs;
  mutable MCFSimplex _solver;
};

template<typename T>
std::ostream& operator<<(std::ostream& cout, const Operator<T>& op) {
  op.print(cout);
  return cout;
}

#endif
