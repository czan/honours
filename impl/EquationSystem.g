grammar EquationSystem;

options {
  language=C;
  output=AST;
  backtrack=true;
}

tokens {
  PLUS = '+' ;
  SUB = '-' ;
  MULT = '*' ;
  COMMA = ';' ;
  GUARD = 'guard' ;
  GREATER_EQUAL = '>=' ;
  QUESTION_MARK = '?' ;
  MCF = 'MCF' ;
  MAXIMUM = 'max' ;
  MINIMUM = 'min' ;
  NEWLINE = '\n' ;
}

equation_system : equation ( NEWLINE! + equation  )* (NEWLINE!)* ;
equation : VARIABLE '='! maxExpr ;

maxExpr : MAXIMUM^ '('! minExpr ( ','! minExpr )* ')'! | minExpr ;
minExpr : MINIMUM^ '('! maxExpr ( ','! maxExpr )* ')'! | expr ;

expr : '(' expr GREATER_EQUAL expr QUESTION_MARK expr ')' -> ^(GUARD expr expr expr)
    | GUARD^ '('! maxExpr ','! maxExpr ','! maxExpr ')'!
    | MCF^ '<'! supplies ','! arcs '>'! '('! maxExpr (','! maxExpr)* ')'!
    | 'add'^ '('! maxExpr ( ','! maxExpr )* ')'!
    | 'sub'^ '('! maxExpr ( ','! maxExpr )* ')'!
    | 'mult'^ '('! maxExpr ( ','! maxExpr )* ')'!
    | term ( (PLUS | MULT | SUB | COMMA)^ expr )* ;

supplies : '['^ NUMBER (','! NUMBER)* ']'! ;
arc : NUMBER ':'^ NUMBER ;
arcs : '['^ arc (','! arc)* ']'! ;

term : NUMBER
     | VARIABLE
     | '-'^ term ;


NUMBER : '-'? (DIGIT)+ ;
VARIABLE: (LETTER) (LETTER | DIGIT | '-' | '\[' | ']' )* ;
WHITESPACE : ( '\t' | ' ' | '\u000C' )+
             {
               $channel = HIDDEN;
             } ;

fragment
DIGIT : '0' .. '9' ;
fragment
LETTER: 'a' .. 'z' ;
