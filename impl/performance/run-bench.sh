#!/bin/bash

NUM=1
COUNT=1

while [[ `echo "$NUM < 100000" | bc` == "1" ]]
do
  echo -n "($NUM,"
  python generate-$1.py $NUM > system.eqns
  echo `../build/main system.eqns 2>&1 >/dev/null | grep Time | egrep -o "[0-9]*\.[0-9]+"` ")"
  NUM=$(($NUM * 2))
  COUNT=$(($COUNT + 1))
done
