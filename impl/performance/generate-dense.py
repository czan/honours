import sys

size = int(sys.argv[1])

for i in xrange(size):
  expr = "max(0"
  for j in xrange(size):
    if j == i:
      continue
    expr += ','
    expr += "x"+str(j)+" + "+str(j)+''
  expr += ")"
  print 'x'+str(i)+' = '+expr
