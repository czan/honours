/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the `ctime_r' function. */
#define HAVE_CTIME_R 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the `gettimeofday' function. */
#define HAVE_GETTIMEOFDAY 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the <limits.h> header file. */
#define HAVE_LIMITS_H 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/times.h> header file. */
#define HAVE_SYS_TIMES_H 1

/* Define to 1 if you have the <sys/time.h> header file. */
#define HAVE_SYS_TIME_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the `times' function. */
#define HAVE_TIMES 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have CBC. */
/* #undef LEMON_HAVE_CBC */

/* Define to 1 if you have CLP. */
/* #undef LEMON_HAVE_CLP */

/* Define to 1 if you have CPLEX. */
/* #undef LEMON_HAVE_CPLEX */

/* Define to 1 if you have GLPK. */
/* #undef LEMON_HAVE_GLPK */

/* Define to 1 if you have long long. */
#define LEMON_HAVE_LONG_LONG 1

/* Define to 1 if you have any LP solver. */
/* #undef LEMON_HAVE_LP */

/* Define to 1 if you have any MIP solver. */
/* #undef LEMON_HAVE_MIP */

/* Define to 1 if you have SOPLEX. */
/* #undef LEMON_HAVE_SOPLEX */

/* The version string */
#define LEMON_VERSION 1.2.3

/* Define to the sub-directory in which libtool stores uninstalled libraries.
   */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "lemon"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "lemon-user@lemon.cs.elte.hu"

/* Define to the full name of this package. */
#define PACKAGE_NAME "LEMON"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "LEMON 1.2.3"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "lemon"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.2.3"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Define to 1 if you can safely include both <sys/time.h> and <time.h>. */
#define TIME_WITH_SYS_TIME 1

/* Define to 1 if your <sys/time.h> declares `struct tm'. */
/* #undef TM_IN_SYS_TIME */

/* Version number of package */
#define VERSION "1.2.3"

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */

/* Define to `__inline__' or `__inline' if that's what the C compiler
   calls it, or to nothing if 'inline' is not supported under any name.  */
#ifndef __cplusplus
/* #undef inline */
#endif

/* Define to `unsigned int' if <sys/types.h> does not define. */
/* #undef size_t */
